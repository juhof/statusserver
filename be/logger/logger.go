package logger

import (
	"fmt"
	"time"
)

type Logger struct{}

func getTimestamp() string {
	return time.Now().Format(time.RFC3339)
}

func (l *Logger) Info(msg string) {
	fmt.Printf("[%s] INFO: %s\n", getTimestamp(), msg)
}

func (l *Logger) Debug(msg string) {
	fmt.Printf("[%s] DEBUG: %s\n", getTimestamp(), msg)
}

func (l *Logger) Error(msg string) {
	fmt.Printf("[%s] ERROR: %s\n", getTimestamp(), msg)
}
func Init() (*Logger, error) {
	return &Logger{}, nil
}
