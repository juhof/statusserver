package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"net/http"
	"regexp"
)

var SERVER *Server

func parseRegex(regex string, target string) string {
	regExp, _ := regexp.Compile(regex)
	regExpResult := regExp.FindStringSubmatch(target)
	if len(regExpResult) != 2 {
		return ""
	}
	return regExpResult[1]
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	SERVER.logger.Info("Redirecting to index")
	http.Redirect(w, r, "/index", http.StatusSeeOther)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("fe/index.html")
	if err != nil {
		SERVER.logger.Error(fmt.Sprintf("%v", err))
		return
	}
	SERVER.logger.Debug("Executing page...")
	linksResponse := LinkResponse{Links: SERVER.getLinks()}
	linksResponseJSON, err := json.Marshal(linksResponse)
	if err != nil {
		SERVER.logger.Error(fmt.Sprintf("%v", err))
	}
	err = t.Execute(w, string(linksResponseJSON))
	if err != nil {
		SERVER.logger.Error(fmt.Sprintf("%v", err))
	}
	SERVER.logger.Debug("Done executing page")
}

func packageHandler(w http.ResponseWriter, r *http.Request) {
	packageNameParam, ok := r.URL.Query()["package_name"]
	if !ok {
		SERVER.logger.Error("Could not get package name from URL")
		errorHandler(w, r, http.StatusNotFound)
		return
	}
	if len(packageNameParam) != 1 {
		SERVER.logger.Info("Received more than one package name parameter")
	}
	packageName := packageNameParam[0]
	if packageName == "" {
		errorHandler(w, r, http.StatusNotFound)
		return
	}
	pkg, err := SERVER.findPackage(packageName)
	if err != nil {
		errorHandler(w, r, http.StatusNotFound)
		SERVER.logger.Error(fmt.Sprintf("%v", err))
		return
	}
	t, err := template.ParseFiles("fe/package.html")
	if err != nil {
		SERVER.logger.Error(fmt.Sprintf("%v", err))
		return
	}
	response := PkgResponse{
		Name:        pkg.name,
		Description: pkg.description,
		Depends:     SERVER.getDependsLink(pkg.name),
		Status:      pkg.status,
		Version:     pkg.version,
		PreDepends:  SERVER.getPreDependsLink(pkg.name),
	}
	responseJSON, err := json.Marshal(response)
	if err != nil {
		SERVER.logger.Error(fmt.Sprintf("%v", err))
		return
	}
	err = t.Execute(w, string(responseJSON))
	if err != nil {
		SERVER.logger.Error(fmt.Sprintf("%v", err))
	}
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	if status == http.StatusNotFound {
		fmt.Fprint(w, "Page not found")
	}
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "favicon.ico")
}

func initServer(address string, port string, statusFile string) *Server {
	return &Server{address: address, port: port, statusFile: statusFile, packages: []Package{}}
}

func main() {
	listenAddress := flag.String("address", "localhost", "Listen address")
	listenPort := flag.String("port", "8080", "Listen port for the server")
	statusFile := flag.String("status", "/var/lib/dpkg/status", "Status file from where package information is read.")
	flag.Parse()

	server := initServer(*listenAddress, *listenPort, *statusFile)
	SERVER = server

	err := server.readPackages()
	if err != nil {
		server.logger.Error(fmt.Sprintf("%v", err))
		return
	}
	server.startServer()
}
