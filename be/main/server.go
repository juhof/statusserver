package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strings"

	logger "statusserver/logger"
)

type Server struct {
	address    string
	port       string
	logger     *logger.Logger
	statusFile string
	packages   []Package
	links      []Link
}

type Package struct {
	name        string
	description string
	depends     []string
	preDepends  []string
	version     string
	status      string
	link        string
}

type PkgResponse struct {
	Name        string
	Description string
	Depends     string
	PreDepends  string
	Version     string
	Status      string
}

type LinkResponse struct {
	Links []string
}

type Link struct {
	name string
	link string
}

func (s *Server) findPackage(pkgName string) (Package, error) {
	for _, pkg := range s.packages {
		if pkg.name == pkgName {
			return pkg, nil
		}
	}
	return Package{}, errors.New(fmt.Sprintf("Package %s not found", pkgName))
}

func (s *Server) getDependsLink(pkgName string) string {
	p, err := s.findPackage(pkgName)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Could not resolve depencies: %v", err))
		return ""
	}
	return s.getDepLinks(pkgName, p.depends)
}

func (s *Server) getPreDependsLink(pkgName string) string {
	p, err := s.findPackage(pkgName)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Could not resolve preDepencies: %v", err))
		return ""
	}
	return s.getDepLinks(pkgName, p.preDepends)
}

func (s *Server) getDepLinks(pkgName string, depencies []string) string {
	links := ""
	for _, dependency := range depencies {
		links += s.getLink(dependency)
	}

	return links
}

func (s *Server) getLink(name string) string {
	pkg, err := s.findPackage(name)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Could not get link: %v", err))
		return name + "<br>"
	}
	return pkg.link
}

func (s *Server) getLinks() []string {
	s.logger.Debug("Getting links...")
	links := []string{}
	for _, pkg := range s.packages {
		links = append(links, pkg.link)
	}
	s.logger.Debug("Done getting links")
	return links
}

func (s *Server) parsePackage(pkgInfo []string) Package {
	newPackage := Package{}
	readingDescription := false
	for _, line := range pkgInfo {
		if readingDescription && parseRegex("^\\s", line) != "" {
			readingDescription = false
		}
		pkgName := parseRegex("Package:\\s(\\S*)", line)
		if pkgName != "" {
			newPackage.name = pkgName
		}
		depends := parseRegex("^Depends:\\s(.*)", line)
		if depends != "" {
			newPackage.depends = s.parseDepends(depends)
		}
		preDepends := parseRegex("^Pre-Depends:\\s(.*)", line)
		if preDepends != "" {
			newPackage.preDepends = s.parseDepends(preDepends)
		}
		version := parseRegex("^Version:\\s(.*)", line)
		if version != "" {
			newPackage.version = version
		}
		status := parseRegex("^Status:\\s(.*)", line)
		if status != "" {
			newPackage.status = status
		}
		description := parseRegex("^Description:\\s(.*)", line)
		if description != "" {
			newPackage.description = description + "."
			readingDescription = true
		}
		if readingDescription {
			description = parseRegex("^(\\s.*)", line)
			if description != "" {
				if description == " ." {
					description = "<br>" // Add HTML line break
				}
				newPackage.description += description
			}
		}
	}

	// Make link for package
	newPackage.link = s.makeLink(newPackage.name)

	return newPackage
}

func (s *Server) makeLink(pkgName string) string {
	escapedPackageName := strings.Replace(pkgName, "+", "%2B", -1) // HTML/URL escape fro character "+".
	return fmt.Sprintf("<a href=\"http://%s:%s/statusserver/package?package_name=%s\">%s</a><br>",
		s.address,
		s.port,
		escapedPackageName,
		pkgName)
}

func (s *Server) parseDepends(dependsInput string) []string {
	var dependencies []string
	depends := strings.Split(dependsInput, ",")
	for _, dependencyRaw := range depends {
		dependency := parseRegex("^\\s*(\\S*)", dependencyRaw)
		if dependency != "" && !stringInSlice(dependency, dependencies) {
			dependencies = append(dependencies, dependency)
		}
	}
	return dependencies
}

func (s *Server) readPackages() error {
	s.logger.Info(fmt.Sprintf("Reading packages from file: %s", s.statusFile))

	statusFile, err := os.Open(s.statusFile)
	if err != nil {
		s.logger.Error(fmt.Sprintf(": %v", err))
		return errors.New(fmt.Sprintf("could not read status file: %v", err))
	}
	defer statusFile.Close() // Close status file when leaving this function.

	readingPkg := false
	pkgInfo := []string{}
	fileScanner := bufio.NewScanner(statusFile)
	for fileScanner.Scan() {
		line := fileScanner.Text()
		if strings.Contains(line, "Package: ") {
			// Start to read new package
			readingPkg = true
		}

		if readingPkg {
			// Add al package information to string array prior to parsing
			pkgInfo = append(pkgInfo, line)
		}

		endOfPkg, _ := regexp.MatchString("^$", line)
		if endOfPkg {
			// Reach the end of a package definition. Proceed to parse information.
			s.packages = append(s.packages, s.parsePackage(pkgInfo))
			readingPkg = false
			pkgInfo = []string{}
		}
	}
	s.packages = append(s.packages, s.parsePackage(pkgInfo)) // Add final package
	s.alphabetizeByName()
	s.logger.Info("Done reading packages")
	return nil
}

func (s *Server) alphabetizeByName() {
	sort.SliceStable(s.packages, func(i, j int) bool {
		return s.packages[i].name < s.packages[j].name
	})
}

func (s *Server) startServer() {
	logger, err := logger.Init()
	if err != nil {
		fmt.Printf("Error while constructing logger: %v", err)
		os.Exit(1)
	}
	s.logger = logger

	// Format listen address
	listenAddress := s.address + ":" + s.port

	// Handle function definitions.
	http.Handle("/", http.FileServer(http.Dir("fe/")))
	http.HandleFunc("/favicon", faviconHandler)
	http.HandleFunc("/statusserver/", indexHandler)
	http.HandleFunc("/statusserver/package", packageHandler)

	// Listen and Serve (non TLS connection)
	s.logger.Info(fmt.Sprintf("Listening to address %s", listenAddress))
	s.logger.Info(fmt.Sprintf("Go to address %s/statusserver", listenAddress))
	log.Fatal(http.ListenAndServe(listenAddress, nil))
}
