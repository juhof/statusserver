#! /bin/bash

Help() 
{
    echo ""
    echo "run.sh: Runs StatusServer"
    echo ""
    echo "Usage:"
    echo ""
    echo "$ ./run.sh [-a|-p|-f] <input> [-h]"
    echo ""
    echo "  -h    Print this help"
    echo "  -a    Listen address. Default: localhost"
    echo "  -p    Listen port. Default: 8080"
    echo "  -f    Package status file to read. Default: /var/lib/dpkg/status"
    echo ""
    echo "Examples:"
    echo ""
    echo "Run with default settings"
    echo "$ ./run.sh"
    echo ""
    echo "Run with parameters"
    echo "$ ./run.sh -a localhost -p 8080 -f resources/status_small"
    echo ""
}
run_command="be/build/bin/main"
while getopts :a:p:f:h OPT; do
    case $OPT in
        a)
            run_command="$run_command -address $OPTARG"
            ;;
        p)
            run_command="$run_command -port $OPTARG"
            ;;
        f)
            run_command="$run_command -status $OPTARG"
            ;;
        h)
            Help
            exit
            ;;
    esac
done

eval "$run_command"
