#! /bin/bash


cd be/

echo "Cleaning previous builds..."
make clean

echo "Building status server..."
make build

return_code=$?
if [[ $return_code -ne 0 ]]; then
    echo "Error while building StatusServer"
else
    echo "Done"
fi