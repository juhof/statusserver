# Status Server: Package Info Finder

- Author: Juho Frigård
- Date: 30.03.2022
- Version: 1.0

## Purpose

This program's function is to display information of known packages in file /var/lib/dpkg/status to the user through a web interface.

## Requirements
### OS: 
- Debian based Linux

### Software
- Make
- Go

## Usage

### Build
```console
./build.sh
```

### Run

```console
run.sh: Runs StatusServer

Usage:

$ ./run.sh [-a|-p|-f] <input> [-h]

  -h    Print this help
  -a    Listen address. Default: localhost
  -p    Listen port: Default: 8080
  -f    Package status file to read, Default: /var/lib/dpkg/status

Examples:

Run with default settings
$ ./run.sh

Run with parameters
$ ./run.sh -a localhost -p 8080 -f resources/status_small
```

__1. Start server__
```console
./run.sh
```
This should print something like this
```console
[2022-03-30T16:57:46+03:00] INFO: Reading packages from file: /var/lib/dpkg/status
[2022-03-30T16:57:48+03:00] INFO: Done reading packages
[2022-03-30T16:57:48+03:00] INFO: Listening to address localhost:8080
[2022-03-30T16:57:48+03:00] INFO: Go to address localhost:8080/statusserver
```

__2. Open web browser and navigate to address *localhost:8080/statusserver*__

View such as this should open:

![statusserver main page example](resources/statusserver.png "Example of statusserver main page")

__3. Click on one of the package names__

Package information opens in a new view such as this:

![package info example](resources/package.png "Example of package information")